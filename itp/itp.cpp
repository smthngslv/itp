#include <iostream>

//Visual C++ 17 
//Ivan Izmailov 

#pragma region Matrix.h

template <typename T>
class Matrix {
public:
	~Matrix();

	Matrix();
    Matrix(const Matrix<T>& matrix);
	Matrix(const uint32_t rows, const uint32_t columns);

	void clear();

	uint32_t    rows() const;
	uint32_t columns() const;
	bool     isEmpty() const;

	Matrix<T>& operator = (const Matrix<T>& matrix);

    T* operator [] (const uint32_t row);
    const T* const operator [] (const uint32_t row) const;

protected:
	static T** getBuffer(const uint32_t rows, const uint32_t columns);
	
    template <typename M>
    friend std::ostream& operator << (std::ostream& ostream, const Matrix<M>& matrix);

    template <typename M>
    friend std::istream& operator >> (std::istream& istream, Matrix<M>& matrix);

	uint32_t rs;
	uint32_t cs;

	T** mx;
};

#pragma region Operators

template<typename T>
Matrix<T>& Matrix<T>::operator = (const Matrix<T>& matrix) {
    if (matrix.isEmpty()) {
        clear(); return *this;
    }

    rs = matrix.rs; // Rows.
    cs = matrix.cs; // Columns.

    mx = getBuffer(rs, cs);

    for (uint32_t r = 0; r < rs; ++r) {
        // Copy a row.
        std::copy(matrix[r], matrix[r] + cs, mx[r]);
    }

    return *this;
}


template<typename T>
T* Matrix<T>::operator [] (const uint32_t row) {
    if (isEmpty()) {
        throw std::invalid_argument("The matrix is empty.");
    }

    if (row >= rs) {
        throw std::out_of_range("Invalid index.");
    }

    return mx[row];
}

template<typename T>
const T* const Matrix<T>::operator[](const uint32_t row) const {
    if (isEmpty()) {
        throw std::invalid_argument("The matrix is empty.");
    }

    if (row >= rs) {
        throw std::out_of_range("Invalid index.");
    }

    return mx[row];
}


template<typename T>
std::ostream& operator << (std::ostream& ostream, const Matrix<T>& matrix) {
    if (matrix.isEmpty()) {
        return ostream;
    }

    for (uint32_t r = 0; r < matrix.rs; ++r) {
        for (uint32_t c = 0; c < matrix.cs; ++c) {
            ostream << matrix[r][c] << (c == matrix.cs - 1 ? "" : " ");
        }

        ostream << std::endl;
    }

    return ostream;
}

template <typename T>
std::istream& operator >> (std::istream& istream, Matrix<T>& matrix) {
    if (!matrix.isEmpty()) {
        matrix.clear();
    }

    istream >> matrix.rs >> matrix.cs;

    matrix.mx = Matrix<T>::getBuffer(matrix.rs, matrix.cs);

    for (uint32_t r = 0; r < matrix.rs; ++r) {
        for (uint32_t c = 0; c < matrix.cs; ++c) {
            istream >> matrix[r][c];
        }
    }

    return istream;
}


template <typename T>
Matrix<T> operator + (const Matrix<T>& left, const Matrix<T>& right) {
    if (left.rows() != right.rows() || left.columns() != right.columns()) {
        throw std::range_error("The sizes of matrices should be equal.");
    }

    Matrix<T> result(left.rows(), left.columns());

    for (uint32_t r = 0; r < result.rows(); ++r) {
        for (uint32_t c = 0; c < result.columns(); ++c) {
            result[r][c] = left[r][c] + right[r][c];
        }
    }

    return result;
}

template <typename T>
Matrix<T> operator - (const Matrix<T>& left, const Matrix<T>& right) {
    if (left.rows() != right.rows() || left.columns() != right.columns()) {
        throw std::range_error("The sizes of matrices should be equal.");
    }

    Matrix<T> result(left.rows(), left.columns());

    for (uint32_t r = 0; r < result.rows(); ++r) {
        for (uint32_t c = 0; c < result.columns(); ++c) {
            result[r][c] = left[r][c] - right[r][c];
        }
    }

    return result;
}

template <typename T>
Matrix<T> operator * (const Matrix<T>& left, const Matrix<T>& right) {
    if (left.columns() != right.rows()) {
        throw std::range_error("The number of rows should be equal to number of columns of the second matrix.");
    }

    Matrix<T> result(left.rows(), right.columns());

    for (uint32_t r = 0; r < result.rows(); ++r) {
        for (uint32_t c = 0; c < result.columns(); ++c) {
            result[r][c] = 0;

            for (uint32_t i = 0; i < left.columns(); ++i) {
                result[r][c] += left[r][i] * right[i][c];
            }
        }
    }

    return result;
}

template <typename T>
Matrix<T> operator * (const Matrix<T>& left, const T right) {
    Matrix<T> result(left.rows(), left.columns());

    for (uint32_t r = 0; r < result.rows(); ++r) {
        for (uint32_t c = 0; c < result.columns(); ++c) {
            result[r][c] = left[r][c] * right;
        }
    }

    return result;
}

template <typename T>
Matrix<T> operator / (const Matrix<T>& left, const T right) {
    Matrix<T> result(left.rows(), left.columns());

    for (uint32_t r = 0; r < result.rows(); ++r) {
        for (uint32_t c = 0; c < result.columns(); ++c) {
            result[r][c] = left[r][c] / right;
        }
    }

    return result;
}

template <typename T>
Matrix<T> operator ~ (const Matrix<T>& left) {
    Matrix<T> result(left.columns(), left.rows());

    for (uint32_t r = 0; r < result.rows(); ++r) {
        for (uint32_t c = 0; c < result.columns(); ++c) {
            result[r][c] = left[c][r];
        }
    }

    return result;
}

#pragma /* Operators. */ endregion 

#pragma /* Matrix.h */ endregion

#pragma region Matrix.cpp

#pragma region Operations

template<typename T>
void Matrix<T>::clear() {
    if (isEmpty()) {
        return;
    }

    // Delete buffer.
    for (uint32_t r = 0; r < rs; ++r) {
        delete[] mx[r];
    }

    delete[] mx;

    rs = 0; // Rows.
    cs = 0; // Columns.

    mx = nullptr;
}

template<typename T>
T** Matrix<T>::getBuffer(const uint32_t rows, const uint32_t columns) {
    T** buff = new T * [rows];

    for (uint32_t i = 0; i < rows; ++i) {
        buff[i] = new T[columns];

        for (uint32_t j = 0; j < columns; ++j) {
            buff[i][j] = (T)0;
        }
    }

    return buff;
}

#pragma /* Operations */ endregion

#pragma region Constructors

template<typename T>
Matrix<T>::~Matrix() {
    clear();
}

template<typename T>
Matrix<T>::Matrix() {
    rs = 0; // Rows.
    cs = 0; // Columns.

    mx = nullptr; // Matrix.
}

template<typename T>
Matrix<T>::Matrix(const Matrix<T>& matrix) : Matrix()
{
    *this = matrix; // Copy by using overload of '=' operator.
}

template<typename T>
Matrix<T>::Matrix(const uint32_t rows, const uint32_t columns) : Matrix() {
    if (rows <= 0 || columns <= 0) {
        return;
    }

    rs = rows;
    cs = columns;

    mx = getBuffer(rs, cs);
}

#pragma /* Constructors */ endregion

#pragma region Properties

template<typename T>
uint32_t Matrix<T>::rows() const {
    return rs;
}

template<typename T>
uint32_t Matrix<T>::columns() const {
    return cs;
}

template<typename T>
bool Matrix<T>::isEmpty() const {
    return mx == nullptr;
}

#pragma /* Properties */ endregion

#pragma /* Matrix.cpp */ endregion

#pragma region SquareMatrix.h

template <typename T>
class SquareMatrix : public Matrix<T> {
public:
    SquareMatrix() : Matrix<T>() { }
    SquareMatrix(uint32_t size) : Matrix<T>(size, size) { }
    SquareMatrix(const SquareMatrix<T>& matrix) : Matrix<T>((Matrix<T>)matrix) { }


    SquareMatrix<T>& operator = (const SquareMatrix<T>& matrix);

    T* operator [] (const uint32_t row);
    const T* const operator [] (const uint32_t row) const;
private:
    template <typename M>
    friend std::istream& operator >> (std::istream& istream, SquareMatrix<M>& matrix);
};

#pragma region Operators

template<typename T>
SquareMatrix<T>& SquareMatrix<T>::operator = (const SquareMatrix<T>& matrix)
{
    Matrix<T>::operator = ((Matrix<T>)matrix);

    return *this;
}

template<typename T>
std::istream& operator >> (std::istream& istream, SquareMatrix<T>& matrix)
{
    if (!matrix.isEmpty()) {
        matrix.clear();
    }

    // Input only one size.
    istream >> matrix.rs;

    // Dementions are the same.
    matrix.cs = matrix.rs;

    // The same as usuall matrix.
    matrix.mx = Matrix<T>::getBuffer(matrix.rs, matrix.cs);

    for (uint32_t r = 0; r < matrix.rs; ++r) {
        for (uint32_t c = 0; c < matrix.cs; ++c) {
            istream >> matrix[r][c];
        }
    }

    return istream;
}

template<typename T>
T* SquareMatrix<T>::operator[](const uint32_t row)
{
    return Matrix<T>::operator[](row);
}

template<typename T>
const T* const SquareMatrix<T>::operator[](const uint32_t row) const
{
    return Matrix<T>::operator[](row);
}

/* ALERT: BELOW MAGIC HAPPENS
   
   // Upcasting.
   Matrix<T>*  left_ptr = &left; 
   Matrix<T>* right_ptr = &right;

   // Compute.
   Matrix<T> result_tmp = *left_ptr + *right_ptr;

   // Downcasting.
   SquareMatrix<T>* result_ptr = (SquareMatrix<T>*)(&result_tmp)

   // Done.
   return *result_ptr;

   //Or, we can write it in short form:
   
          // Downcasting.          //Upcasting    //Compute     //Upcasting          
   return *(SquareMatrix<T>*)(&(*((Matrix<T>*) & left) + *((Matrix<T>*) & right)));

   // Magic. Defently.

   // UPD: Stepic doesn't allow take address of temporary varable. It hurts :c
   // Magic doesn't come.
*/

template <typename T>
SquareMatrix<T> operator + (const SquareMatrix<T>& left, const SquareMatrix<T>& right) {
    Matrix<T> tmp_result = *((Matrix<T>*) & left) + *((Matrix<T>*) & right);

    return *(SquareMatrix<T>*)(&(*((Matrix<T>*) & left) + *((Matrix<T>*) & right)));

    return *(SquareMatrix<T>*)(&tmp_result);
}

template <typename T>
SquareMatrix<T> operator - (const SquareMatrix<T>& left, const SquareMatrix<T>& right) {
    Matrix<T> tmp_result = *((Matrix<T>*) & left) - *((Matrix<T>*) & right);


    return *(SquareMatrix<T>*)(&tmp_result);
}

template <typename T>
SquareMatrix<T> operator * (const SquareMatrix<T>& left, const SquareMatrix<T>& right) {
    Matrix<T> tmp_result = *((Matrix<T>*) & left) * *((Matrix<T>*) & right);


    return *(SquareMatrix<T>*)(&tmp_result);
}

template <typename T>
SquareMatrix<T> operator * (const SquareMatrix<T>& left, const T right) {
    Matrix<T> tmp_result = *((Matrix<T>*) & left) * right;


    return *(SquareMatrix<T>*)(&tmp_result);
}

template <typename T>
SquareMatrix<T> operator / (const SquareMatrix<T>& left, const T right) {
    Matrix<T> tmp_result = *((Matrix<T>*) & left) / right;


    return *(SquareMatrix<T>*)(&tmp_result);
}

template <typename T>
SquareMatrix<T> operator ~ (const SquareMatrix<T>& left) {
    Matrix<T> tmp_result = ~ *((Matrix<T>*) & left);


    return *(SquareMatrix<T>*)(&tmp_result);
}

#pragma /* Operators */ endregion

#pragma /* SquareMatrix.h */ endregion

#pragma region IdentityMatrix.h

template <typename T>
class IdentityMatrix : public SquareMatrix<T> {
public:
    IdentityMatrix(uint32_t size) : SquareMatrix<T>(size) {
        for (uint32_t i = 0; i < size; ++i) {
            SquareMatrix<T>::mx[i][i] = (T)1;
        }
    }

    IdentityMatrix(const IdentityMatrix<T>& matrix) : SquareMatrix<T>((SquareMatrix<T>)matrix) { };

    // Only read.
    const T* const operator [] (const uint32_t row) const;
private:
    // We cannot clear identity.
    virtual void clear() {}

    // We cannot assign anything to IdentytyMatrix.
    virtual void operator = (const IdentityMatrix<T>& matrix) { }
};

#pragma region Operators

template<typename T>
const T* const IdentityMatrix<T>::operator[](const uint32_t row) const
{
    return SquareMatrix<T>::operator[](row);
}

#pragma /* Operators */ endregion

#pragma /* IdentityMatrix.h */ endregion

#pragma region EliminationMatrix.h

template <typename T>
class EliminationMatrix : public IdentityMatrix<T> {
public:
    EliminationMatrix(uint32_t size, uint32_t row, uint32_t column, T value) : IdentityMatrix<T>(size) {
        IdentityMatrix<T>::mx[row][column] = value;
    }

    EliminationMatrix(const EliminationMatrix<T>& matrix) : IdentityMatrix<T>((IdentityMatrix<T>)matrix) { };
};

#pragma /* EliminationMatrix.h */ endregion

#pragma region PermutationMatrix.h

template <typename T>
class PermutationMatrix : public IdentityMatrix<T> {
public:
    PermutationMatrix(uint32_t size, uint32_t f_row, uint32_t s_row) : IdentityMatrix<T>(size) {
        std::swap(IdentityMatrix<T>::mx[f_row], IdentityMatrix<T>::mx[s_row]);
    }

    PermutationMatrix(const PermutationMatrix<T>& matrix) : IdentityMatrix<T>((IdentityMatrix<T>)matrix) { };
};

#pragma /* PermutationMatrix.h */ endregion

int main() {
    SquareMatrix<int> A;
    
    std::cin >> A;

    EliminationMatrix<int> E(2, 1, 0, -A[1][0]);
    PermutationMatrix<int> P(2, 1, 0);

    std::cout << IdentityMatrix<int>(3);

    std::cout << E;
    std::cout << (A = E * A);

    std::cout << P;
    std::cout << P * A;
    
    return 0;
}