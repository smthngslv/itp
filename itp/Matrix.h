#pragma once

#include <cstdint>
#include <iostream>

namespace itp{

	template <typename T>
	class Matrix {
	public:
		~Matrix();

		Matrix();
		Matrix(uint32_t rows, uint32_t columns);

		void clear();

		uint32_t    rows() const;
		uint32_t columns() const;
		bool     isEmpty() const;
		
		Matrix<T>& operator = (const Matrix& matrix);

	private:
		static T** getBuffer(uint32_t rows, uint32_t columns);
		/*
		template <typename T>
		friend std::ostream& operator << (std::ostream& ostream, const Matrix<T>& matrix);
		template <typename T>
		friend std::istream& operator >> (std::istream& istream,	   Matrix<T>& matrix);
		*/
		uint32_t rs;
		uint32_t cs;

		T** mx;
	};

	/*
	template <typename T> 
	Matrix<T>& operator ! (const Matrix<T>& matrix);

	template <typename T> 
	Matrix<T>& operator ~ (const Matrix<T>& matrix);

	template <typename T> 
	Matrix<T>& operator + (const Matrix<T>& left, const Matrix<T>& right);

	template <typename T> 
	Matrix<T>& operator - (const Matrix<T>& left, const Matrix<T>& right);

	template <typename T> 
	Matrix<T>& operator * (const Matrix<T>& left, const Matrix<T>& right);
	

	template<typename T>
	std::ostream& operator << (std::ostream& ostream, const Matrix<T>& matrix)
	{
		if (matrix.isEmpty()) {
			return ostream;
		}

		for (uint32_t r = 0; r < matrix.rs; ++r) {
			for (uint32_t c = 0; c < matrix.cs; ++c) {
				ostream << matrix.mx[r][c] << " ";
			}

			ostream << std::endl;
		}

		return ostream;
	}
	*/
}

