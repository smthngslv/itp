#include "Matrix.h"
#include <iostream>

using namespace itp;

#pragma region Constructors

template<typename T>
Matrix<T>::~Matrix()
{
    clear();
}

template<typename T>
Matrix<T>::Matrix()
{
    rs = 0; // Rows.
    cs = 0; // Columns.

    mx = nullptr; // Matrix.
}


template<typename T> 
Matrix<T>::Matrix(uint32_t rows, uint32_t columns)
{
    rs = rows;
    cs = columns;

    mx = getBuffer(rs, cs);
}

#pragma /* Constructors */ endregion

template<typename T>
void Matrix<T>::clear()
{
    if (isEmpty()) {
        return;
    }

    // Delete buffer.
    for (int r = 0; r < rs; ++r) {
        delete[] mx[r];
    }

    delete[] mx;

    rs = 0; // Rows.
    cs = 0; // Columns.
}

template<typename T>
T** Matrix<T>::getBuffer(uint32_t rows, uint32_t columns)
{
    T** buff = new T * [rows];

    for (uint32_t i = 0; i < rows; ++i)
    {
        buff[i] = new T[columns];
    }

    return buff;
}

#pragma region Properties

template<typename T>
uint32_t Matrix<T>::rows() const
{
    return rs;
}

template<typename T>
uint32_t Matrix<T>::columns() const
{
    return cs;
}

template<typename T>
bool Matrix<T>::isEmpty() const
{
    return mx == nullptr;
}

#pragma /* Properties */ endregion

#pragma region Operators

template<typename T>
Matrix<T>& Matrix<T>::operator = (const Matrix& matrix)
{
    if (matrix.isEmpty()) {
        clear(); return *this;
    }

    rs = matrix.rs; // Rows.
    cs = matrix.cs; // Columns.

    mx = getBuffer(rs, cs);

    for (int r = 0; r < rs; ++r) {
        // Copy a row.
        std::copy(matrix.mx[r], matrix.mx[r] + cs, mx[r]);
    }

    return *this;
}


/*
template <typename T>
std::istream& operator >> (std::istream& istream, Matrix<T>& matrix)
{
    if (!matrix.isEmpty()) {
        matrix.clear();
    }

    istream >> matrix.rs >> matrix.cs;

    matrix.mx = itp::Matrix::getBuffer(matrix.rs, matrix.cs);

    for (uint32_t r = 0; r < matrix.rs; ++r) {
        for (uint32_t c = 0; c < matrix.cs; ++c) {
            istream >> matrix.mx[r][c];
        }
    }

    return istream;
}
*/
#pragma /* Operators. */ endregion 
